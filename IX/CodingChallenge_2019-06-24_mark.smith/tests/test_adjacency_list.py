# 2019-06-24, Mark Smith
"""
    Summary:
    Unittests for functions used to generate an adjacency list for a position in a item_list that can be represented as a 
    wrapping grid. i.e.

                                              ----------------
                                              | 0| 1| 2| 3| 4|
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11] -> | 5| 6| 7| 8| 9|
                                              |10|11|  |  |  |
                                              ----------------
"""

from collections import OrderedDict
import unittest
from logic.adjacency_list import *


class TestAdjacencyList(unittest.TestCase):


    def test_get_adjacency_list_for_position(self):
        
        # Test the adjacency list is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        list_pos = 0
        expected_result = [['u', 10], ['r', 1], ['d', 5], ['l', 4]]
        result = get_adjacency_list_for_position(item_list, row_length, list_pos)
        self.assertEqual(result, expected_result)

        # Test the adjacency list is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        list_pos = 2
        expected_result = [['u', 7], ['r', 3], ['d', 7], ['l', 1]]
        result = get_adjacency_list_for_position(item_list, row_length, list_pos)
        self.assertEqual(result, expected_result)


    def test_find_top_edge_pos(self):
        
        # Test the correct top edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 0
        col_num = 0
        expected_result = 10
        result = find_top_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct top edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 0
        col_num = 2
        expected_result = 7
        result = find_top_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct top edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 1
        col_num = 0
        expected_result = 0
        result = find_top_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct top edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 2
        col_num = 0
        expected_result = 5
        result = find_top_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct top edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 2
        col_num = 3
        expected_result = None
        result = find_top_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct top edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 10
        col_num = 3
        expected_result = None
        result = find_top_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)


    def test_find_right_edge_pos(self):
        
        # Test the correct right edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 0
        col_num = 0
        expected_result = 1
        result = find_right_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct right edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 0
        col_num = 4
        expected_result = 0
        result = find_right_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct right edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 2
        col_num = 0
        expected_result = 11
        result = find_right_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct right edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 2
        col_num = 1
        expected_result = 10
        result = find_right_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)


        # Test the correct right edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 2
        col_num = 3
        expected_result = None
        result = find_right_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)


    def test_find_bottom_edge_pos(self):

        # Test the correct bottom edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 0
        col_num = 0
        expected_result = 5
        result = find_bottom_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct bottom edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 1
        col_num = 0
        expected_result = 10
        result = find_bottom_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct bottom edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 2
        col_num = 0
        expected_result = 0
        result = find_bottom_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct bottom edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 0
        col_num = 2
        expected_result = 7
        result = find_bottom_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct bottom edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 1
        col_num = 2
        expected_result = 2
        result = find_bottom_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct bottom edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 2
        col_num = 2
        expected_result = 2
        result = find_bottom_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

    def test_find_left_edge_pos(self):
        
        # Test the correct left edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 0
        col_num = 0
        expected_result = 4
        result = find_left_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct left edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 0
        col_num = 1
        expected_result = 0
        result = find_left_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct left edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 1
        col_num = 0
        expected_result = 9
        result = find_left_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct left edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 1
        col_num = 2
        expected_result = 6
        result = find_left_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct left edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 2
        col_num = 0
        expected_result = 11
        result = find_left_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)

        # Test the correct left edge position is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 2
        col_num = 1
        expected_result = 10
        result = find_left_edge_pos(item_list, row_length, row_num, col_num)
        self.assertEqual(result, expected_result)


    def test_calc_col_num(self):

        # Test the correct col_num is returned
        item_pos = 0
        row_length = 5
        expected_result = 0
        result = calc_col_num(item_pos, row_length)
        self.assertEqual(result, expected_result)

        # Test the correct col_num is returned
        item_pos = 4
        row_length = 5
        expected_result = 4
        result = calc_col_num(item_pos, row_length)
        self.assertEqual(result, expected_result)

        # Test the correct col_num is returned
        item_pos = 5
        row_length = 5
        expected_result = 0
        result = calc_col_num(item_pos, row_length)
        self.assertEqual(result, expected_result)

        # Test the correct col_num is returned
        item_pos = 9
        row_length = 5
        expected_result = 4
        result = calc_col_num(item_pos, row_length)
        self.assertEqual(result, expected_result)

        # Test the correct col_num is returned
        item_pos = 10
        row_length = 5
        expected_result = 0
        result = calc_col_num(item_pos, row_length)
        self.assertEqual(result, expected_result)

        # Test the correct col_num is returned
        item_pos = 11
        row_length = 5
        expected_result = 1
        result = calc_col_num(item_pos, row_length)
        self.assertEqual(result, expected_result)


    def test_calc_row_num(self):
        
        # Test the correct row_num is returned
        item_pos = 0
        row_length = 5
        expected_result = 0
        result = calc_row_num(item_pos, row_length)
        self.assertEqual(result, expected_result)

        # Test the correct row_num is returned
        item_pos = 4
        row_length = 5
        expected_result = 0
        result = calc_row_num(item_pos, row_length)
        self.assertEqual(result, expected_result)

        # Test the correct row_num is returned
        item_pos = 5
        row_length = 5
        expected_result = 1
        result = calc_row_num(item_pos, row_length)
        self.assertEqual(result, expected_result)

        # Test the correct row_num is returned
        item_pos = 9
        row_length = 5
        expected_result = 1
        result = calc_row_num(item_pos, row_length)
        self.assertEqual(result, expected_result)

        # Test the correct row_num is returned
        item_pos = 10
        row_length = 5
        expected_result = 2
        result = calc_row_num(item_pos, row_length)
        self.assertEqual(result, expected_result)


    def test_calc_row_col_num_to_position(self):

        # Test that correct position is returned        
        row_num = 0
        col_num = 0
        row_length = 5
        expected_result = 0
        result = calc_row_col_num_to_position(row_num, col_num, row_length)
        self.assertEqual(result, expected_result)

        # Test that correct position is returned
        row_num = 0
        col_num = 4
        row_length = 5
        expected_result = 4
        result = calc_row_col_num_to_position(row_num, col_num, row_length)
        self.assertEqual(result, expected_result)

        # Test that correct position is returned
        row_num = 1
        col_num = 0
        row_length = 5
        expected_result = 5
        result = calc_row_col_num_to_position(row_num, col_num, row_length)
        self.assertEqual(result, expected_result)

        # Test that correct position is returned
        row_num = 1
        col_num = 4
        row_length = 5
        expected_result = 9
        result = calc_row_col_num_to_position(row_num, col_num, row_length)
        self.assertEqual(result, expected_result)

        # Test that correct position is returned
        row_num = 2
        col_num = 0
        row_length = 5
        expected_result = 10
        result = calc_row_col_num_to_position(row_num, col_num, row_length)
        self.assertEqual(result, expected_result)

        # Test that None is returned on an invalid column
        row_num = 0
        col_num = 5
        row_length = 5
        expected_result = None
        result = calc_row_col_num_to_position(row_num, col_num, row_length)
        self.assertEqual(result, expected_result)


    def test_calc_row_count(self):

        # Test the calculation of the row count
        list_length = 5
        row_length = 5
        expected_result = 1        
        result = calc_row_count(list_length, row_length)
        self.assertEqual(result, expected_result)

        # Test the calculation of the row count
        list_length = 6
        row_length = 5
        expected_result = 1
        result = calc_row_count(list_length, row_length)
        self.assertNotEqual(result, expected_result)

        # Test the calculation of the row count
        list_length = 6
        row_length = 7
        expected_result = 1
        result = calc_row_count(list_length, row_length)
        self.assertEqual(result, expected_result)

        # Test the calculation of the row count
        list_length = 12
        row_length = 5
        expected_result = 3
        result = calc_row_count(list_length, row_length)
        self.assertEqual(result, expected_result)


    def test_get_row_segment(self):
        
        # Test that the row is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 0
        expected_result = ["Q", "W", "E", "R", "T"]
        result = get_row_segment(item_list, row_length, row_num)
        self.assertEqual(result, expected_result)


        # Test that the row is returned
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 1
        expected_result = ["Y", "U", "I", "B", "P"]
        result = get_row_segment(item_list, row_length, row_num)
        self.assertEqual(result, expected_result)        

        # Test retrieving a out of bounds row returns empty list
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 5
        row_num = 5
        expected_result = []
        result = get_row_segment(item_list, row_length, row_num)
        self.assertEqual(result, expected_result)   

        # Test different size row
        item_list = ["Q", "W", "E", "R", "T", "Y", "U", "I", "B", "P", "A", "S"]
        row_length = 3
        row_num = 0
        expected_result = ["Q", "W", "E"]
        result = get_row_segment(item_list, row_length, row_num)
        self.assertEqual(result, expected_result)   
        
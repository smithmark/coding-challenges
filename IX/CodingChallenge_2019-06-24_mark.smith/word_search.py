# 2019-06-24, Mark Smith

import sys
from logic.breadth_first_iterative import execute_breadth_first_search_iterative
from util.input_util import read_file_path_to_json_obj
from util.output_util import output_json_obj_to_file


def main(argv):
    """
    Summary:
        Main entry point into the application

    Args:
        argv (list): list of input arguments
    """

    # Validate input arguments
    if len(argv) < 3:
        raise Exception('Valid Syntax: python3 word_search.py input_file_path output_file_path')

    # Get input parameters
    input_file_path = argv[1]
    output_file_path = argv[2]

    # Read input file path into json object
    json_obj = read_file_path_to_json_obj(input_file_path)

    for json_item in json_obj:
        # For each item in the input object try to find the path
        try:
            find_path(json_item)
        
        except Exception as e:
            json_item['error'] = True
            json_item['error_message'] = 'Fatal error when finding path'
        
    # Output the updated json object to the output file
    output_json_obj_to_file(output_file_path, json_obj)

def find_path(json_dict):
    """
    Summary:
        Process a json object and set the result path and distance values.

    Args:
        json_dict (dict): dictionary representing the input search parameters
    """

    starting_focus = json_dict['startingFocus']
    word = json_dict['word']
    alphabet = json_dict['alphabet']
    row_length = json_dict['rowLength']

    # Execute the search, returns search path list with items in the form of [key_press, list_position]
    search_path_list = execute_breadth_first_search_iterative(alphabet, row_length, word, starting_focus)

    # Initialize the distance and complete path
    path = []
    distance = 0

    # For each item in the search path list
    for item in search_path_list:
        key_press = item[0]
        # Append the key pressed to the path
        path.append(key_press)

        # If key_press was not p then it was a move, increment distance
        if key_press != 'p':
            distance += 1

    # Set the path and distance parameters
    json_dict['path'] = path
    json_dict['distance'] = distance


if __name__ == '__main__':
    argv = sys.argv
    main(argv)
# 2019-06-24, Mark Smith
"""
    Summary:
    Provides utility functions to write an object from the main application to an output file.
"""

import json
import io
import os


def output_json_obj_to_file(
    file_path,
    json_obj,
    append_data=False
):
    """
    Summary:
        Output a json object to the provided file path
    
    Args:
        file_path (str): path of the file to output the json obj
        json_obj (obj): object containing data that can be output to json string
        append_data (bool, optional): flag to overwrite or append data to file
    """

    json_string = json_obj_to_json_string(json_obj)
    f = None

    if append_data:
        f = io.open(file_path,'a',encoding='utf8')
    else:
        f = io.open(file_path,'w',encoding='utf8')

    f.write(json_string)
    f.close()

class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        print('-------------------------------------------')
        print('type(obj): {}'.format(type(obj)))
        print('obj: {}'.format(obj))
        print('-------------------------------------------')
        if isinstance(obj, complex):
            return [obj.real, obj.imag]
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)

def json_obj_to_json_string(
    json_obj
):
    """
    Summary:
        Output a json object to a string.

    Args:
        json_obj (obj): object containing data that can be output to json string
    
    Returns:
        json_string (str): string representation of the object
    """

    for json_item in json_obj:

        # Cannot be sure if the automated tests take any valid json or ONLY the specified format
        # Manually stringify the alphabet and path lists to match the input format
        alphabet_str = manual_list_stringify(json_item['alphabet'])
        json_item['alphabet'] = alphabet_str

        if 'path' in json_item:
            path_str = manual_list_stringify(json_item['path'])
            json_item['path'] = path_str

    json_string = json.dumps(json_obj, indent=4, sort_keys=True)
    
    json_string = json_string.replace('\\"', '"')
    json_string = json_string.replace('"[', '[')
    json_string = json_string.replace(']"', ']')

    return json_string


def manual_list_stringify(
    json_list
):
    """
    Summary:
        Output a json object to a string.

    Args:
        json_list (list): list containing data to me manually stringified
    
    Returns:
        json_string (str): string representation of the list
    """

    json_string = ''
    for item in json_list:
        if len(json_string) < 1:
            json_string += '["{}"'.format(item)
        else:
            json_string += ', "{}"'.format(item)    


    json_string += ']'

    return json_string
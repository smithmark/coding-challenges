# 2019-06-24, Mark Smith
"""
    Summary:
    Provides utility functions to read an input file and convert it into an object usable by the main application.
"""

import os
import io
import json


def read_file_path_to_json_obj(
    input_file_path
):
    """
    Summary:
        Reads a input file and returns a 'json' object

    Args:
        input_file_path (str): location of the input file
    
    Returns:
        json_obj (obj): json object
    
    Raises:
        Exception: file_path does not exist
    """

    if not os.path.exists(input_file_path):
        raise Exception('input_file_path does not exist')

    input_file = io.open(input_file_path,'r',encoding='utf8')

    json_string = input_file.read()
    input_file.close()

    json_obj = json_string_to_json_obj(json_string)
    return json_obj


def json_string_to_json_obj(
    json_string
):
    """
    Summary:
        Converts a json_string to a 'json' object

    Args:
        json_string (str): string representing a json object
    
    Returns:
        json_obj (obj): json object
    """

    json_obj = json.loads(json_string)
    
    return json_obj
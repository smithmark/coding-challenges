# 2019-06-24, Mark Smith

import sys
import unittest
from tests.test_adjacency_list import TestAdjacencyList

def main(argv):
    """
    Summary:
        Main entry point into the testing application

    Args:
        argv (list): list of input arguments
    """

    # Call the unit tests
    unittest.main()

if __name__ == '__main__':
    argv = sys.argv
    main(argv)
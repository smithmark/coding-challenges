# 2019-06-24, Mark Smith
"""
    Summary:
    Provides functions to execute a iterative breadth first search to find the path between characters spelling an input word
    on a virtual keyboard.

    Assumptions:
        - Keyboard only contains unique characters and is case sensitive
        - Output will be a an array of objects containing the distance and array of strings which represent the motions taken
            to type the word.

"""

from logic.adjacency_list import get_adjacency_list_for_position


def execute_breadth_first_search_iterative(
    item_list,
    row_length,
    word,
    starting_focus
):
    """
    Summary:
        Searches for a path on a virtual keyboard between keys to spell out a provided input word.
        Divides the input into segments (start_item, end_item) that can be searched using a iterative
        breadth first search algorithm.
    
    Args:
        item_list (list): list of items to be searched
        row_length (int): length of item rows
        word (str): complete input search word
        starting_focus (str): starting position item
    """

    # Add the start position to the search word to make segmentation easier
    adjusted_word = '{}{}'.format(starting_focus, word)

    # Initialize a list to store the complete search path for all segments
    search_path = []

    # Iterate through the word to create segments that are easier to search
    for x in range(0, (len(adjusted_word) - 1)):

        # Segment starting item
        start_item = adjusted_word[x]

        # Check for start_item existance
        if start_item not in item_list:
            msg = 'Unable to find start_item {} in item_list'.format(start_item)
            raise Exception(msg)

        # Segment starting position
        start_pos = item_list.index(start_item)

        # Segment ending item
        end_item = adjusted_word[x + 1]

        # Check for end_item existance
        if end_item not in item_list:
            msg = 'Unable to find end_item {} in item_list'.format(end_item)
            raise Exception(msg)

        # Segment ending position
        end_pos = item_list.index(end_item)

        # Search item_list to find segment path between start_pos and end_pos
        segment_path = breadth_first_search_iterative(item_list, row_length, start_pos, end_pos)

        # Check for path existance
        if segment_path is None or len(segment_path) < 1:
            msg = 'Unable to find path between [start_item: {}, start_pos: {}] and [end_item: {}, end_pos: {}]'.format(
                start_item,
                start_pos,
                end_item,
                end_pos
            )
            raise Exception(msg)

        # Add segment path to complete search path
        # First position of segment path is the starting position, trim it off
        search_path += segment_path[1:]
    
    # Return the complete search path
    return search_path


def breadth_first_search_iterative(
    item_list,
    row_length,
    start_pos,
    end_pos
):
    """
    Summary::
        Searches for a path between two positions in the input item list using a iterative Breadth First Search (BFS) algorithm. 
    
    Args:
        item_list (list): list of items to be searched
        row_length (int): length of item rows
        start_pos (int): starting position for the search
        end_pos (int): ending position for the search
    
    Returns:
        path (list): list of moves between the start_pos and end_pos
    """

    # Initialize visited_list with starting position
    # visited_list tracked visited positions to prevent duplication of effort / possible infinite loop
    visited_list = [start_pos]

    # current position objects are simple lists with the 0 value containing the last move / keypress and the 1 value
    #  containing the absolute list position
    curr_pos_obj = ['START', start_pos]

    # Initialize path_queue with a path that only contains the starting position 
    path_queue = []
    path_queue.append([curr_pos_obj])
    
    # While path_queue is non-empty
    while path_queue:

        # Get the first path in the queue to search
        curr_path = path_queue[0]
        
        # Dequeue the current path
        path_queue = path_queue[1:]

        # Get the current position object to make code more readable
        curr_pos_obj = curr_path[-1]

        # Goal check
        if curr_pos_obj[1] == end_pos:
            # If goal found add a keypress ('p') and return the current path
            curr_path.append(['p', end_pos])
            return curr_path

        # Get list of adjacent edges following the pre-defined rules
        adj_list = get_adjacency_list_for_position(item_list, row_length, curr_pos_obj[1])
        
        # For each adjacent position
        for adj_pos in adj_list:
            # If the position has already been visited then continue
            if adj_pos[1] in visited_list:
                continue

            # Mark the position as visited
            visited_list.append(adj_pos[1])
            
            # Create a new path with the adjacent position
            new_path = list(curr_path)
            new_path.append(adj_pos)

            # Add the new path to the queue
            path_queue.append(new_path)

    # No path to goal was found
    return None

# 2019-06-24, Mark Smith
"""
    Summary:
    Functions used to generate an adjacency list for a position in a item_list that can be represented as a 
    wrapping grid. i.e.

                                              ----------------
                                              | 0| 1| 2| 3| 4|
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11] -> | 5| 6| 7| 8| 9|
                                              |10|11|  |  |  |
                                              ----------------
    Assumptions:
        - Item grids are left justified
        - All rows except for the last row must be completely filled
        - Items on the edge of the grid will have adjacent positions that wrap back to the same row / column.
            Bottom wraps to top, top wraps to bottom, left wraps to right, right wraps to left.
        - Row / columns with a single item should wrap back to itself.
        - Each position has four adjacent edges (positions): up, down, left, right. There are no diagonal edges / positions.

"""

from collections import OrderedDict


def get_adjacency_list_for_position(
    item_list,
    row_length,
    list_pos
):
    """
    Summary:
        Get the list of adjacent items for a given position
    Args:
        item_list (list): list of items that can be represented as a grid
        row_length (int): length of item rows
        list_pos (int): position of item in list to find adjacent items for
    
    Returns:
        adj_list (list): list of adjacent positions
    """

    # Calculate the row and column number using the absolute list position
    row_num = calc_row_num(list_pos, row_length)
    col_num = calc_col_num(list_pos, row_length)

    # Initialize adjacency list
    adj_list = []

    # Find top edge adjacency and add to adjacency list if it does not already exist
    top_pos = find_top_edge_pos(item_list, row_length, row_num, col_num)
    if top_pos != list_pos and top_pos not in adj_list:
        adj_list.append(['u', top_pos])

    # Find right edge adjacency and add to adjacency list if it does not already exist
    right_pos = find_right_edge_pos(item_list, row_length, row_num, col_num)
    if right_pos != list_pos and right_pos not in adj_list:
        adj_list.append(['r', right_pos])

    # Find bottom edge adjacency and add to adjacency list if it does not already exist
    bottom_pos = find_bottom_edge_pos(item_list, row_length, row_num, col_num)
    if bottom_pos != list_pos and bottom_pos not in adj_list:
        adj_list.append(['d', bottom_pos])

    # Find left edge adjacency and add to adjacency list if it does not already exist
    left_pos = find_left_edge_pos(item_list, row_length, row_num, col_num)
    if left_pos != list_pos and left_pos not in adj_list:
        adj_list.append(['l', left_pos])

    return adj_list


def find_top_edge_pos(
    item_list,
    row_length,
    row_num,
    col_num
):
    """
    Summary:
        Find the top edge adjacent position for the current position

    Args:
        item_list (list): list of items that can be represented as a grid
        row_length (int): length of item rows
        row_num (TYPE): row number of the current position
        col_num (TYPE): column number of the current position
    
    Returns:
        adj_pos (int): result adjacent position
    """

    # Column remains constant
    # Determine new adjacent row
    adj_row_num = row_num

    # Get the row count
    row_count = calc_row_count(len(item_list), row_length)

    # Invalid row
    if adj_row_num >= row_count:
        return None

    # Get the row segment
    row = get_row_segment(item_list, row_length, adj_row_num)

    # Invalid column
    # Note: because get_row_segment returns an empty list this check actually handles both
    #   invalid row_num and invalid col_num. But explicitly checking the row first saves
    #   the time of segmenting the row and improves perf overall.
    if col_num >= len(row):
        return None

    if row_num > 0:
        # Non-wrapping case
        adj_row_num = row_num - 1
    else:
        # Adjacency wrapping case
        
        # Set the adjacent row to the bottom row
        adj_row_num = row_count

        # Slightly convuluted method of finding the top wrap, since assumption 2 indicates that only the bottom row
        # can be partially filled. Could be replaced with two separate checks
        while True:
            # Subtract from the adjacent row number to go 'up a row'
            adj_row_num -= 1

            # Get the row segment
            row = get_row_segment(item_list, row_length, adj_row_num)

            # If the length of the row is greater than the column number we have found the adjacency
            # and do not have to continue
            if len(row) > col_num:
                break
    
    # Calculate the list position for the adjacency
    adj_pos = calc_row_col_num_to_position(adj_row_num, col_num, row_length)
    return adj_pos


def find_right_edge_pos(
    item_list,
    row_length,
    row_num,
    col_num
):
    """
    Summary:
        Find the right edge adjacent position for the current position

    Args:
        item_list (list): list of items that can be represented as a grid
        row_length (int): length of item rows
        row_num (TYPE): row number of the current position
        col_num (TYPE): column number of the current position
    
    Returns:
        adj_pos (int): result adjacent position
    """

    # Determine new adjacent column 
    # Row remains constant
    adj_col_num = col_num

    # Get the current row
    row = get_row_segment(item_list, row_length, row_num)
    
    # Invalid column for the row
    if adj_col_num >= len(row):
        return None

    # Add 1 to move adjacent row to the right
    adj_col_num += 1

    # If moving to the right pushes past the length of the current row then wrap around to start of row
    if adj_col_num >= len(row):
        adj_col_num = 0

    # Calculate the list position for the adjacency
    adj_pos = calc_row_col_num_to_position(row_num, adj_col_num, row_length)
    return adj_pos


def find_bottom_edge_pos(
    item_list,
    row_length,
    row_num,
    col_num
):
    """
    Summary:
        Find the bottom edge adjacent position for the current position

    Args:
        item_list (list): list of items that can be represented as a grid
        row_length (int): length of item rows
        row_num (TYPE): row number of the current position
        col_num (TYPE): column number of the current position
    
    Returns:
        adj_pos (int): result adjacent position
    """

    # Column remains constant
    # Determine new adjacent row
    adj_row_num = row_num
    
    row_count = calc_row_count(len(item_list), row_length)

    if row_num == row_count - 1:
        # Non-wrapping case
        adj_row_num = 0
    else:
        # Adjacency wrapping case
        adj_row_num += 1

        # Get the current row
        row = get_row_segment(item_list, row_length, adj_row_num)

        # If the current row does not contain a value for this column then wrap to top

        if len(row) <= col_num:
            adj_row_num = 0

    # Calculate the list position for the adjacency
    adj_pos = calc_row_col_num_to_position(adj_row_num, col_num, row_length)
    return adj_pos


def find_left_edge_pos(
    item_list,
    row_length,
    row_num,
    col_num
):
    """
    Summary:
        Find the left edge adjacent position for the current position

    Args:
        item_list (list): list of items that can be represented as a grid
        row_length (int): length of item rows
        row_num (TYPE): row number of the current position
        col_num (TYPE): column number of the current position
    
    Returns:
        adj_pos (int): result adjacent position
    """

    # Determine new adjacent column 
    # Row remains constant
    adj_col_num = col_num

    # Get current row
    row = get_row_segment(item_list, row_length, row_num)

    # Move adjacent position to the left
    adj_col_num -= 1

    # If adjacent column is outside the grid then wrap to the right most position
    if adj_col_num < 0:
        adj_col_num = len(row) - 1

    # Calculate the list position for the adjacency
    adj_pos = calc_row_col_num_to_position(row_num, adj_col_num, row_length)
    return adj_pos


def calc_col_num(
    item_pos,
    row_length
):
    """
    Summary:
        Calculate the column number for a input item position.

    Args:
        item_pos (int): position of item in the item_list
        row_length (int): length of item rows
    
    Returns:
        col_num(int): column number of the item position
    """

    col_num = item_pos % row_length

    return col_num


def calc_row_num(
    item_pos,
    row_length
):
    """
    Summary:
        Calculate the row number for a input item position.

    Args:
        item_pos (int): position of item in the item_list
        row_length (int): length of item rows
    
    Returns:
        row_num(int): row number of the item position
    """

    # Get the floored row_num
    row_num = int(item_pos / row_length)

    # Get the column number
    col_num = calc_col_num(item_pos, row_length)

    # Add 1 to row_number
    if col_num > (row_length - 1):
        row_num += 1

    return row_num


def calc_row_col_num_to_position(
    row_num,
    col_num,
    row_length
):
    """
    Summary:
        Calculate the list postion from input grid coordinates

    Args:
        row_num (int): row number of item
        col_num (int): column number of item
        row_length (int): length of item rows
    
    Returns:
        list_pos (int): list position of item
    """

    # Return None if invalid column
    if col_num >= row_length:
        return None

    # Calculate the list position from the grid coordinates
    list_pos = row_num * row_length + col_num
    
    return list_pos


def calc_row_count(
    list_length,
    row_length
):
    """
    Summary:
        Calculate the number of grid rows from a item list
    Args:
        list_length (int): length of the item list
        row_length (int): length of item rows
    
    Returns:
        row_count (int): number of rows in the grid
    """

    # Get floored row count
    row_count = int(list_length / row_length)

    # If a remainder exists then add one for the partially filled row
    if list_length % row_length > 0:
        row_count += 1

    return row_count


def get_row_segment(
    item_list,
    row_length,
    row_num
):
    """
    Summary:
        Gets a segment of the item_list representing a particular row

    Args:
        item_list (list): list of items that can be represented as a grid
        row_length (int): length of item rows
        row_num (int): number of the row to segment
    
    Returns:
        row_segment (list): list of items representing the row segment
    """

    # Calculate the starting and ending positions of the row
    row_start_pos = row_length * row_num
    row_col_pos = row_length * (row_num + 1)

    # Segment the row
    row_segment = item_list[row_start_pos : row_col_pos]
    return row_segment
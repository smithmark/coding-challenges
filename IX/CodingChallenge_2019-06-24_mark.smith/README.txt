----------------------------------------------------------------------------
Purpose
----------------------------------------------------------------------------
Application finds the shortest path on a virtual keyboard to spell out a word meeting the specifications as presented in the provided KW Interview Assignment.docx. A file containing a string representation of a list of json objects must be provided as input to initiate the search. Multiple json objects inside of this file will cause the search to be executed multiple times. Application will output the result to the provided output file as a string representation.


----------------------------------------------------------------------------
Execution
----------------------------------------------------------------------------
- Application requires Python 3 and no special libraries or packages. 
- A valid make package is required to use the make file commands. 
- Application written and tested on Debian 9.


Application syntax:
    - python3 ./word_search.py <input_file_path> <output_file_path>

Makefile syntax:
    - make all
        - Execute unit tests, make run and make run_test_input

    - make run
        - Execute application using the provided json input file

    - make run_test_input
        - Execute application using a modified json input file with additional tests

    - make test
        - Execute provided unit tests for the application

    - make clean
        - Remove json files from the base directory.


----------------------------------------------------------------------------
Notes
----------------------------------------------------------------------------
- This application uses adjacency lists and a breadth first search algorithm to find the shortest path between nodes, represented as grid positions. 

- The application was written with the goal of providing clearer, more readable code that is easier to test over providing the most concise or fastest way of completing the task.

- Unit tests for the functions provided in ./logic/adjacency_list.py have been provided. These do not represent an exhaustive list of possible testing scenarios and the application code coverage is not 100%. 

- Application logic is divided into several components inside of the logic, util and tests folders. 
    - Utils contains functions for reading and writing of the files.
    - Logic contains functions for executing the search and generating the adjacency lists.
    - Tests contains the (not exhaustive) set of unit tests for the application

- Two sample input files have been provided in the input_files folder.
    - input_provided.json is the sample input from the assignment.
    - input_test.json is the sample input from the assignment with additional scenarios to further test the application.
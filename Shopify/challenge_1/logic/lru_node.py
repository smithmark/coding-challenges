class LRUCache(object):

    def __init__(self, key, value):
        self._key = key
        self._value = value

    def key(self):
        return self._key

    def value(self):
        return self._value
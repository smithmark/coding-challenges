

class LRUCache(object):

    def __init__(self, max_size):
        self._max_size = max_size
        self.clear()

    def max_size(self):
        return self._max_size

    def write(self, key, value):
        print('LRUCache.write')

        if key is None:
            raise Exception('write - Invalid key')

        if value is None:
            raise Exception('write - Invalid value')

        if key in self._data:
            self._data[key] = value
        else:
            if len(self._data.keys()) >= self.max_size():
                # Pop the first element out of cache
                first_key = self._key_list[0]
                self.delete(first_key)

            self._data[key] = value
            self._key_list.append(key)
            

        return value


    def read(self, key):
        print('LRUCache.read')

        if key is None:
            raise Exception('read - Invalid key')

        if key in self._data:
            return self._data[key]

        return None

    def delete(self, key):
        print('LRUCache.delete')

        if key not in self._data:
            raise Exception('key not in data')

        self._data.pop(key)

        if key in self._key_list:
            key_index = self._key_list.index(key)

            self._key_list.pop(key_index)


    def clear(self):
        print('LRUCache.clear')
        self._data = {}
        self._key_list = []

    def count(self):
        # print('LRUCache.count')

        return len(self._data)

    def __repr__(self):
        # print()
        # msg = 'LRUCache.__repr__'
        # re

        # return msg
        return str(self._data)

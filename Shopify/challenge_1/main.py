# 2019-07-03, Mark Smith

import sys
from logic.lru_cache import LRUCache


def main(argv):
    """
    Summary:
        Main entry point into the application

    Args:
        argv (list): list of input arguments
    """

    print('main.main(argv)')

    lru_cache = LRUCache(max_size=3)
    # print(lru_cache.max_size())
    print(lru_cache)

    result = lru_cache.write('key1', 'val1')
    print(result)
    print(lru_cache)

    result = lru_cache.write('key2', 'val2')
    print(result)
    print(lru_cache)

    result = lru_cache.write('key3', 'val3')
    print(result)
    print(lru_cache)

    result = lru_cache.write('key4', 'val4')
    print(result)
    print(lru_cache)
    
    print(lru_cache.read('key1'))

    print(lru_cache.read('key4'))

    print(lru_cache)

    result = lru_cache.write('key5', 'val5')

    result = lru_cache.write('key6', 'val6')

    print(lru_cache)

    print(lru_cache.count())

    print(lru_cache.clear())

    print(lru_cache)


if __name__ == '__main__':
    argv = sys.argv
    main(argv)
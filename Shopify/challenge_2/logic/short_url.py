import hashlib

class ShortUrl(object):

    def __init__(self, user_id, original_url):
        self._user_id = user_id
        self._original_url = original_url

        self._short_url = self.calculate_short_url()


    def user_id(self):
        return self._user_id

    def original_url(self):
        return self._original_url

    def short_url(self):
        return self._short_url


    def calculate_short_url(self):
        print('calculate_short_url')
        # return self._original_url

        # Generate a hash

        m = hashlib.sha256()

        m.update(self._original_url.encode('utf-8'))
        url_hash = m.digest()

        short_url = str(url_hash[:10])
        # short_url = short_url[:10]
        print('short_url: {}'.format(short_url))





        return short_url




from logic.short_url import ShortUrl

class UrlShortener(object):

    def __init__(self):
        self._url_data = {}

    def add(self, user_id, original_url):
        if user_id is None:
            raise Exception('invalid data - user_id')

        if original_url is None:
            raise Exception('invalid data - original_url')



        short_url_obj = ShortUrl(user_id, original_url)
        
        if self.get(short_url_obj.short_url()) is None:
            # Add to the list
            self._url_data[short_url_obj.short_url()] = short_url_obj
        else:
            raise Exception('investigate hash collission')


        return short_url_obj

    def get(self, short_url):
        if short_url is None:
            raise Exception('invalid short_url')

        if short_url in self._url_data:
            return self._url_data[short_url]

        return None



    def click_short_url(self, short_url):
        short_url = self.get(short_url)

        if short_url != None:
            # Log the metadata for tracking
            # pass the original url as a redirect later
            return short_url.original_url()

        return None


    def __repr__(self):
        msg = 'UrlShortener'

        return msg
        
# 2019-07-03, Mark Smith

import sys
from logic.url_shortener import UrlShortener

def main(argv):
    """
    Summary:
        Main entry point into the application

    Args:
        argv (list): list of input arguments
    """

    print('main.main(argv)')

    url_shortener = UrlShortener()

    short_url_obj = url_shortener.add(user_id=1, original_url='www.google.ca')

    print('short_url_obj.short_url(): {}'.format(short_url_obj.short_url()))


    result = url_shortener.click_short_url(short_url=short_url_obj.short_url())

    print(result)

    # database_obj.lookup(the_url_hash)




if __name__ == '__main__':
    argv = sys.argv
    main(argv)
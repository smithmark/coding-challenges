Code Challenge: Checkout
========================

## Purpose

Applicants for the Software Developer role at Wave must complete the following challenge, and submit a solution.

The purpose of this exercise is to create something that we can work on together during the on-site interview.

The sample application is based on Python and Django (and Django REST Framework), but we do NOT ask you to be familiar with this particular stack.

## Instructions: Code Review

We ask applicants to review our sample code "Checkout." It's not a perfect implementation, or rather, we would like you to identify potential issues/bugs (if any), make comments, and suggest improvements. The goal of this review is to make sure we're shipping code that is performant, easy to build on in the future, and does right by our customers.

Code review is a crucial step in Wave's software delivery pipeline. Every code change must get approved from at least one other developer before it can be deployed to production. If you are not familiar with the GitHub style of code reviews, here's [an example](https://github.com/django/django/pull/9151) of an open source project that demonstrates the format of code reviews at Wave.

Although the example above shows the general workflow of a code review, here are a few things you __do not__ need to worry about in this exercise:

- Syntax, best practices, and issues related to the specific implementation of Python, Django or third party libraries. If any of the tools used in the codebase is new to you, it will probably be useful to understand what the lines are trying to do, but you don't have to wonder questions such as "is there a better way to do this?" or "do we need those brackets because we are on Python 3.6.5?"
- Code style and other standards that are specific to this project. Most (if not all) Python code at Wave follow the PEP8 standard and we have automated checks for it. Also, you don't need to point out inconsistencies around implementation for the same type of operations (e.g. ORM usage)

A rule of thumb when you wonder if you should comment on something is asking yourself "how do I contribute to the code as a non-Python developer who joined the company a week ago?"

__We ask that you not spend more than a few hours on this challenge__ for the following reasons:

- We value your time spent on initial screening and are happy to discuss your submission further in the on-site interview.
- At Wave, it generally takes a developer abount 30 minutes to an hour to perform one round of code review for pull requests of similar sizes, and we believe that brevity is a criterion for high quality code reviews.

The final submission should be a text file that contains your comments on the code, in the following format:

> [FILENAME]
>
> [LINE_NUMBER]: [YOUR COMMENT]
>
> [ANOTHER_LINE_NUMBER]: [ANOTHER COMMENT]

You may also leave general comments about the code at the end if they are not directly related to specific lines or files.

Steps to complete the challenge:

1. Read the rest of this file
2. Read the source files (use the "Files" section as a guide)
3. Complete the comment file in the format specified above
4. Email your comment file to dev.careers@waveapps.com

## Evaluation

Wave values the following attributes in its software:

1. Correctness. Wave relies heavily on automated testing for correctness (currently we have no dedicated manual testing resources), and in code review, we need to make sure that the code along with automated test results can give us a high level of confidence on correctness.
1. Ease of Use. Our products should be straightforward to use for the consumers, and the definition of "consumers" goes beyond users of our web apps. In the context of APIs, the front end developers are our consumers.
1. Performance. While we don't prematurely optimize for performance, we should avoid practices that often cause inefficiency.
1. Maintainability. Code should be organized in a way that's easy to understand, change and test.

At Wave, a code review start after all automated tests pass and is the last step before the change is merged into master and deployed to prod, so your submission will be evaluated based on whether it helps improve any of the attributes above.

__Note that we do NOT evaluate the submission based on your experience with Python and Django.__ If you are not sure if something needs to be pointed out because it may simply be how Python or Django works, be explicit about it and point it out anyways - you may even treat it as a learning experience by asking a question, and we will be happy to discuss that during the interview :smiley:

## The App - "Checkout"

We are building an online payment processing system. It's a simpler version of our "Checkout" app which enables merchants to accept payments from customers.

Each "checkout" is created for one item that the merchant has in stock. One or more payments can be made towards the same checkout, and every payment created for that checkout always has the same amount as the unit price of that item.

Actual payments are processed with provided credit card information. We use an external/third-party API for the payment authorization, which is simulated using pseudo-random number generator as the actual behaviour of a credit card processing service is unpredictable from our perspective. You may treat the code in `third-party.py` as the source code that runs on a remote web service.

1. The merchant creates a "checkout"
2. The customer makes a payment against the checkout
3. The app communicates with the external API to authorize the payment

The app comes with other features:

- Returns a list of all existing checkouts
- Returns a list of payments associated with a checkout record
- "Report" -- returns the total amount paid using the app

During your code review, you may assume that this app hasn’t been released to the public yet and feel free to provide feedback on some of the fundamental aspects of this app.

## Prerequisites

This application uses the following platform/libraries:

- Python 3
- Django 2.0
- Django REST Framework

## Installation

Run the following command to install required libraries:

    $ pip install -r requirements.txt

## Setting up the Database

You can set up the SQLite database by running:

    $ python manage.py migrate
    
You should only need to do this once unless you modify the `db.sqlite3` file.

## Running the App

You can run the application locally by running:

    $ python manage.py runserver

All API endpoints can be access through a web browser, or you may interact with them via tools such as Postman or curl.

### Test

To run unit tests:

    $ python manage.py test

This will run 6 tests [source](checkout/api/tests.py).

### Web Server

To run the web server on the local machine:

    $ python manage.py runserver

## Files

Files marked with *(Review)* should be reviewed.

| File               | Description                                               |
| ------------------ | ------------------------------------------------------    |
| checkout/          |                                                           |
| - api/             |                                                           |
| -- \_\_init\_\_.py | (Empty file used for Python's package path)               |
| -- models.py       | *(Review)* Database                                       |
| -- tests.py        | *(Review)* Test cases for the app                         |
| -- third_party.py  | "Third-party" service that authorizes payments            |
| -- views.py        | *(Review)* Processes HTTP requests, and returns responses |
| - checkout/        |                                                           |
| -- \_\_init\_\_.py | (Empty file used for Python's package path)               |
| -- settings.py     | Configuration for the app                                 |
| -- urls.py         | URL (paths/routes) configuration                          |
| -- wsgi.py         |                                                           |
| - manage.py        | Command-line utility for this app (Django)                |
| README.md          | This file                                                 |
| requirements.txt   | Setting file used to install required libraries (pip)     |

## Notes

- Prices are kept in cents

import re
from datetime import date
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import (
    Checkout,
    Payment,
)
from api.third_party import (
    PaymentAuthorizationService,
    PaymentAuthorizationError,
)


class CheckoutListAPIView(APIView):
    def get(self, request):
        description = request.query_params.get("description", "")
        if len(description) == 0:
            all_checkouts = Checkout.objects.all()
        else:  # Filter by description if description is not empty
            all_checkouts = Checkout.objects.filter(description=description)

        if all_checkouts.count() == 1:
            return Response(all_checkouts[0].pk, status=status.HTTP_200_OK)

        response_json = {
            checkout.pk: checkout.description
            for checkout in all_checkouts
        }
        return Response(response_json, status=status.HTTP_200_OK)

    def post(self, request):
        try:
            price = int(request.data["price"])
            description = request.data["description"]
        except (KeyError, ValueError):
            return Response("Invalid price/description", status=status.HTTP_400_BAD_REQUEST)

        created_checkout = Checkout.objects.create(description=description, price=price)

        response_json = {
            "id": created_checkout.pk,
            "description": created_checkout.description,
            "price": created_checkout.price,
        }
        return Response(response_json, status=status.HTTP_200_OK)


class CheckoutAPIView(APIView):
    def get(self, request, checkout_id):
        try:
            checkout = Checkout.objects.get(pk=checkout_id)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        # TODO: Include price in response
        response_json = {
            "id": checkout.pk,
            "description": checkout.description,
            "price": checkout.price,
        }
        return Response(response_json, status=status.HTTP_200_OK)

    def delete(self, request, checkout_id):
        count = Checkout.objects.filter(pk=checkout_id).update(active=False)
        if count < 1:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(status=status.HTTP_204_NO_CONTENT)


class PaymentAPIView(APIView):
    RE_CREDIT_CARD_NUMBER = re.compile("[0-9]{16}")

    def get(self, request, checkout_id):
        all_data = Payment.objects.filter(checkout_id=checkout_id)

        response_json = {
            row.pk: row.date.isoformat()
            for row in all_data
        }
        return Response(response_json, status=status.HTTP_200_OK)

    def post(self, request, checkout_id):
        try:
            checkout = Checkout.objects.get(pk=checkout_id)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if not checkout.active:
            return Response(status=status.HTTP_412_PRECONDITION_FAILED)

        try:
            credit_card_number = request.data["credit_card_number"]
            if not self.RE_CREDIT_CARD_NUMBER.fullmatch(credit_card_number):
                raise ValueError()
        except (KeyError, ValueError):
            return Response("Invalid credit card", status=status.HTTP_400_BAD_REQUEST)

        payment_amount = checkout.price

        payment_auth_service = PaymentAuthorizationService()
        try:
            payment_auth_service.authorize_payment(credit_card_number=credit_card_number, payment_amount=payment_amount)
        except PaymentAuthorizationError:
            return Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)

        created_payment = Payment.objects.create(checkout_id=checkout_id, date=date.today(), credit_card_number=credit_card_number)

        response_json = {
            "id": created_payment.pk,
            "checkout_id": created_payment.checkout.pk,
            "date": created_payment.date.isoformat(),
        }
        return Response(response_json, status=status.HTTP_200_OK)


class ReportAPIView(APIView):
    def get(self, request):
        all_data = Payment.objects.all()
        total = 0
        for row in all_data:
            price = Checkout.objects.get(pk=row.checkout_id).price
            total += price

        return Response({"total": total}, status=status.HTTP_200_OK)

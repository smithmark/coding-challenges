import random
from uuid import uuid4


class PaymentAuthorizationError(Exception):
    pass


class PaymentAuthorizationService(object):

    def authorize_payment(self, credit_card_number, payment_amount):
        if random.randint(1, 2) > 1:
            raise PaymentAuthorizationError

        return uuid4()

from django.db import models


class Checkout(models.Model):
    description = models.TextField()
    price = models.IntegerField()
    active = models.BooleanField(default=True)


class Payment(models.Model):
    checkout = models.ForeignKey(Checkout, on_delete=models.PROTECT)
    date = models.DateField()
    credit_card_number = models.CharField(max_length=16)

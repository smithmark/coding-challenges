import datetime
from rest_framework import status
from rest_framework.test import APITestCase

from api.models import Checkout, Payment


class CheckoutListTest(APITestCase):
    def setUp(self):
        Checkout.objects.create(
            description="sample checkout",
            price=100
        )
        Checkout.objects.create(
            description="another checkout",
            price=200
        )

    def test_get(self):
        response = self.client.get('/checkout/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_get__by_description(self):
        response = self.client.get('/checkout/?description=another checkout')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, 2)

    def test_post(self):
        data = {'description': 'sample checkout 2', 'price': 200}
        response = self.client.post('/checkout/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(Checkout.objects.count(), 3)


class CheckoutTest(APITestCase):
    def setUp(self):
        checkout = Checkout.objects.create(
            description="sample checkout",
            price=100
        )
        self.checkout_id = checkout.id

    def test_get(self):
        response = self.client.get('/checkout/{}/'.format(self.checkout_id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_delete(self):
        response = self.client.delete('/checkout/{}/'.format(self.checkout_id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class PaymentTest(APITestCase):
    def setUp(self):
        checkout = Checkout.objects.create(
            description="sample checkout",
            price=100
        )
        Payment.objects.create(
            checkout=checkout,
            date=datetime.date.today(),
            credit_card_number='4111111111111111'
        )
        self.checkout_id = checkout.id

    def test_get(self):
        response = self.client.get('/checkout/{}/payment/'.format(self.checkout_id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_post(self):
        data = {'credit_card_number': '4111111111111111'}
        response = self.client.post(
            '/checkout/{}/payment/'.format(self.checkout_id),
            data, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(Payment.objects.count(), 2)


class ReportTest(APITestCase):
    def test_success(self):
        checkout_1 = Checkout.objects.create(
            description="sample checkout 1",
            price=100
        )
        checkout_2 = Checkout.objects.create(
            description="sample checkout 2",
            price=10
        )
        Payment.objects.create(
            checkout=checkout_1,
            date=datetime.date.today(),
            credit_card_number='4111111111111111'
        )
        Payment.objects.create(
            checkout=checkout_2,
            date=datetime.date.today(),
            credit_card_number='4111111111111111'
        )

        response = self.client.get(
            '/report/',
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["total"], 110)

    def test_no_payment(self):
        checkout_1 = Checkout.objects.create(
            description="sample checkout 1",
            price=100
        )
        checkout_2 = Checkout.objects.create(
            description="sample checkout 2",
            price=10
        )
        response = self.client.get(
            '/report/',
            format='json'
        )
